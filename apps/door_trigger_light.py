"""Door triggered lights"""

import appdaemon.plugins.hass.hassapi as hass


class DoorTriggeredLight(hass.Hass):
    """Light which is triggered by a door opening and turns off a certain
    number of seconds after the door closes."""

    def initialize(self):
        """Initialise the app"""
        self.doors = self.args["doors"]
        self.light = self.args["light"]
        self.post_close_time = self.args["post_close_time"]
        self.brightness = self.args["brightness"]

        self.timer = None
        for door in self.doors:
            self.listen_state(self.door_callback, door)

    def door_callback(self, entity, attribute, old, new, kwargs):
        """Door event callback"""
        if self.timer is not None:
            self.cancel_timer(self.timer)
        if new == "on":
            self.log("Door opened, turning on light")
            self.turn_on(self.light, brightness_pct=self.brightness)
        else:
            self.log("Door closed, starting shutoff timer")
            self.timer = self.run_in(self.timeout_callback, self.post_close_time)

    def timeout_callback(self, kwargs):
        """Timer callback"""
        self.log("Shutoff timer expired")
        self.timer = None
        self.turn_off(self.light)
