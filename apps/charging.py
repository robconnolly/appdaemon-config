"""Automated Battery Charging"""

from datetime import datetime, timedelta
from tinydb import TinyDB, Query
import appdaemon.plugins.hass.hassapi as hass
import pytimeparse as ptp


class PeriodicCharger(hass.Hass):
    """Charges devices periodically"""

    def initialize(self):
        """Initialise the app"""
        self.switch = self.args["switch"]
        self.charge_time = timedelta(ptp.parse(self.args["charge_time"]))
        self.charge_interval = timedelta(ptp.parse(self.args["charge_interval"]))
        self.listen_state(self.switch_callback, self.switch)

        self.database = TinyDB("db.json")
        self.chargers = Query()
        records = self.database.search(self.chargers.name == self.name)
        if len(records) == 1:
            self.state = records[0]
        else:
            self.state = {"name": self.name, "charge_complete": False}

        self.run_every(
            self.start_charging,
            self.next_start_time(),
            self.charge_interval.total_seconds(),
        )

    def next_start_time(self):
        """Calculate the next start time"""
        if not self.state["charge_complete"]:
            return datetime.now() + timedelta(seconds=5)
        start = self.state["last_charge_time"] + self.charge_interval
        if start < datetime.now():
            return datetime.now() + timedelta(seconds=5)
        return start

    def switch_callback(self, entity, attribute, old, new, kwargs):
        """Switch event callback"""
        if new == "on":
            self.log("Switch state changed to on, scheduling stop event")
            self.run_in(self.stop_charging, self.charge_time.total_seconds())
            self.state["last_start_time"] = datetime.now()
            self.state["charge_complete"] = False
            self.database.update(self.state, self.chargers.name == self.name)

    def start_charging(self, kwargs):
        """Start charging"""
        self.log("Starting toothbrush charge cycle")
        self.turn_on(self.switch)

    def stop_charging(self, kwargs):
        """Stop charging"""
        self.log("Toothbrush charge cycle complete")
        self.turn_off(self.switch)
        self.state["charge_complete"] = True
        self.database.update(self.state, self.chargers.name == self.name)
